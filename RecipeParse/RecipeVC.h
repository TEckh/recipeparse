//
//  RecipeVC.h
//  RecipeParse
//
//  Created by Trevor Eckhardt on 6/18/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeVC : UIViewController
@property (nonatomic, strong) NSDictionary* recipe;

@end
