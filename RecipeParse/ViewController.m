//
//  ViewController.m
//  RecipeParse
//
//  Created by Trevor Eckhardt on 6/18/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>
#import "NewRecipeVC.h"
#import "RecipeVC.h"

@interface ViewController ()

@end

@implementation ViewController

NSArray* array;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
//    testObject[@"foo"] = @"bar";
//    [testObject saveInBackground];
    
    PFQuery *query = [PFQuery queryWithClassName:@"RecipeTable"];
    //[query selectKeys:@[@"Title", @"Description"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *array, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %lu recipes.", array.count);
            // Do something with the found objects
            for (PFObject *object in array) {
                NSLog(@"%@", object.objectId);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
    //REST GET REQUEST
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://api.parse.com/1/classes/Recipes" ]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"rFjjlJaraWDSiDX05ij7IBfLFIW660JPk3pOk4YW" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:@"JPRmTsq9Nbj8imxKvFuRq0vxVQ6BwdC0rKySKe1l" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    NSURLResponse* ReqResponse;
    NSData* requestHandle = [NSURLConnection sendSynchronousRequest:request returningResponse:&ReqResponse error:nil];
    NSString* reqReply = [[NSString alloc] initWithBytes:[requestHandle bytes] length:[requestHandle length] encoding:NSASCIIStringEncoding];
    NSLog(@"Request Reply: %@", reqReply);
    
    UITableView* tv = [[UITableView alloc]init];
    tv.delegate = self;
    tv.dataSource = self;
    [self.view addSubview:tv];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTouched:)];
    // Do any additional setup after loading the view, typically from a nib.
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = [array objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    RecipeVC* rec = [[RecipeVC alloc] init];
    rec.recipe = [array objectAtIndex:indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)addTouched:(id)sender{
    NewRecipeVC* nrvc = [[NewRecipeVC alloc] init];
    [self.navigationController pushViewController:nrvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
