//
//  NewRecipeVC.m
//  RecipeParse
//
//  Created by Trevor Eckhardt on 6/18/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "NewRecipeVC.h"

@interface NewRecipeVC ()

@end

@implementation NewRecipeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView* headView = [UIView new];
    headView.translatesAutoresizingMaskIntoConstraints = NO;
    headView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:headView];
    
    
    UILabel* lbl = [[UILabel alloc] init];
    lbl.translatesAutoresizingMaskIntoConstraints = NO;
    lbl.font = [UIFont systemFontOfSize:20.0];
    lbl.numberOfLines = 0;
    lbl.preferredMaxLayoutWidth = 150.0;
    lbl.backgroundColor = [UIColor darkGrayColor];
    [headView addSubview:lbl];
    
    UITextField* tf = [[UITextField alloc] init];
    tf.translatesAutoresizingMaskIntoConstraints = NO;
    tf.font = [UIFont systemFontOfSize:15.0];
    [self.view addSubview:tf];
    
    NSDictionary* views = NSDictionaryOfVariableBindings(headView, lbl, tf);
    NSDictionary* metrics = @{@"pad":@10};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[headView]|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[headView]|" options:0 metrics:metrics views:views]];
    
    [headView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[lbl]->=0-[tf]-10-|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
    
    NSString* lblText = lbl.text;
    NSString* tfText = tf.text;
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@""]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"rFjjlJaraWDSiDX05ij7IBfLFIW660JPk3pOk4YW" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:@"JPRmTsq9Nbj8imxKvFuRq0vxVQ6BwdC0rKySKe1l" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    NSDictionary* dict = @{@"Title":lblText};
    dict = @{@"Description":tfText};
    
    NSError* error;
    NSData* dataFromDict = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    [request setHTTPBody:dataFromDict];
    
    NSURLResponse* reqRes;
    NSData* reqHandle = [NSURLConnection sendSynchronousRequest:request returningResponse:&reqRes error:nil];
    NSString* reqReply = [[NSString alloc] initWithBytes:[reqHandle bytes] length:[reqHandle length] encoding:NSASCIIStringEncoding];
    NSLog(@"Returned reply: %@", reqReply);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
