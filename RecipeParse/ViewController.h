//
//  ViewController.h
//  RecipeParse
//
//  Created by Trevor Eckhardt on 6/18/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end

