//
//  RecipeVC.m
//  RecipeParse
//
//  Created by Trevor Eckhardt on 6/18/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "RecipeVC.h"

@interface RecipeVC ()

@end

@implementation RecipeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationRecieved:) name:@"recipeChanged" object:nil];
    
    UIView* headView = [UIView new];
    headView.translatesAutoresizingMaskIntoConstraints = NO;
    headView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:headView];
    
    
    UILabel* lbl = [[UILabel alloc] init];
    lbl.translatesAutoresizingMaskIntoConstraints = NO;
    lbl.font = [UIFont systemFontOfSize:20.0];
    lbl.numberOfLines = 0;
    lbl.preferredMaxLayoutWidth = 150.0;
    lbl.text = [self.recipe objectForKey:@"Title"];
    lbl.backgroundColor = [UIColor darkGrayColor];
    [headView addSubview:lbl];
    
    UITextField* tf = [[UITextField alloc] init];
    tf.translatesAutoresizingMaskIntoConstraints = NO;
    tf.font = [UIFont systemFontOfSize:15.0];
    tf.text = [self.recipe objectForKey:@"Description"];
    [self.view addSubview:tf];
    
    NSDictionary* views = NSDictionaryOfVariableBindings(headView, lbl, tf);
    NSDictionary* metrics = @{@"pad":@10};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[headView]|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[headView]|" options:0 metrics:metrics views:views]];
    
    [headView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[lbl]->=5-[tf]-10-|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
    
}

- (NSDictionary*) notificationRecieved:(NSNotification *)n {
    NSDictionary* myRecipe = n.object;
    return myRecipe;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
